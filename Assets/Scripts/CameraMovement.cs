﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour
{

    [SerializeField]
    float speed;
    [SerializeField]
    float rotationDuration;
    [SerializeField]
    float lerpRotationStep;
    [SerializeField]
    float lerpRotationThreshold;
    [SerializeField]
    float screenEdgeMovementSpeed;

    [SerializeField]
    Vector2 topRightBound;
    [SerializeField]
    Vector2 bottomLeftBound;

    void Start()
    {
        if (speed == 0)
        {
            speed = 2f;
        }
    }
    void Update()
    {
        float hAxis = Input.GetAxis("Horizontal");
        float vAxis = Input.GetAxis("Vertical");
        if (hAxis != 0 || vAxis != 0)
        {
            transform.Translate(hAxis * speed, 0f, vAxis * speed);
        }
        else
        {
            if (Input.mousePosition.x == 0)
            {
                transform.Translate(-screenEdgeMovementSpeed, 0f, 0f);
            }
            if (Input.mousePosition.x == Screen.width)
            {
                transform.Translate(screenEdgeMovementSpeed, 0f, 0f);
            }
            if (Input.mousePosition.y == 0)
            {
                transform.Translate(0f, 0f, -screenEdgeMovementSpeed);
            }
            if (Input.mousePosition.y == Screen.height)
            {
                transform.Translate(0f, 0f, screenEdgeMovementSpeed);
            }
        }

        if (topRightBound != Vector2.zero)
        {
            if (transform.position.x > topRightBound.x)
            {
                transform.position = new Vector3(topRightBound.x, transform.position.y, transform.position.z);
            }
            if (transform.position.z > topRightBound.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, topRightBound.y);
            }
        }
        if (bottomLeftBound != Vector2.zero)
        {
            if (transform.position.x < bottomLeftBound.x)
            {
                transform.position = new Vector3(bottomLeftBound.x, transform.position.y, transform.position.z);
            }
            if (transform.position.z < bottomLeftBound.y)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, bottomLeftBound.y);
            }
        }

        if (Input.GetKeyUp(KeyCode.Period))
        {
            RotateAroundYAxis(-45);
        }
        if (Input.GetKeyUp(KeyCode.Comma))
        {
            RotateAroundYAxis(45);
        }
        if (Input.GetKeyUp(KeyCode.L))
        {
            RotateAroundYAxis2(-45);
        }
        if (Input.GetKeyUp(KeyCode.K))
        {
            RotateAroundYAxis2(45);
        }

    }

    bool isRotating = false;
    void RotateAroundYAxis(float degrees)
    {
        if (isRotating == false)
        {
            Quaternion startingRotation = transform.localRotation;
            Quaternion targetRotation = Quaternion.Euler(startingRotation.eulerAngles.x, startingRotation.eulerAngles.y + degrees, startingRotation.eulerAngles.z);
            StartCoroutine(SmoothRotation(startingRotation, targetRotation));
        }
    }
    void RotateAroundYAxis2(float degrees)
    {
        if (isRotating == false)
        {
            Quaternion startingRotation = transform.localRotation;
            Quaternion targetRotation = Quaternion.Euler(startingRotation.eulerAngles.x, startingRotation.eulerAngles.y + degrees, startingRotation.eulerAngles.z);
            StartCoroutine(SmoothRotation2(startingRotation, targetRotation, rotationDuration));
        }
    }

    IEnumerator SmoothRotation(Quaternion startingRotation, Quaternion targetRotation)
    {
        //float startinT = Time.time;
        isRotating = true;
        while (Mathf.Abs(transform.rotation.eulerAngles.y - targetRotation.eulerAngles.y) > lerpRotationThreshold)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, lerpRotationStep);
            yield return new WaitForSeconds(0.01f);
        }
        transform.rotation = targetRotation;
        //Debug.Log("Lerp1 " + (Time.time - startinT));
        isRotating = false;
    }
    IEnumerator SmoothRotation2(Quaternion startingRotation, Quaternion targetRotation, float time)
    {
        //float startinT = Time.time;
        isRotating = true;
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            transform.rotation = Quaternion.Lerp(startingRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
        transform.rotation = targetRotation;
        //Debug.Log("Lerp2 " + (Time.time - startinT));
        isRotating = false;
    }
}



