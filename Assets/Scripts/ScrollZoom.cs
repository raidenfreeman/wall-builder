﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class ScrollZoom : MonoBehaviour {
    
    void Awake()
    {
        cameraComponent = this.GetComponent<Camera>();
    }

    Camera cameraComponent;

	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            cameraComponent.fieldOfView -= 5;
            if(cameraComponent.fieldOfView<10)
            {
                cameraComponent.fieldOfView = 10;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            cameraComponent.fieldOfView += 5;
            if (cameraComponent.fieldOfView > 70)
            {
                cameraComponent.fieldOfView = 70;
            }
        }
    }
}
