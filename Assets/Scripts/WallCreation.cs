﻿using UnityEngine;
using System.Collections;
using System;

public class WallCreation : MonoBehaviour
{

    [SerializeField]
    GameObject Wall;
    [SerializeField]
    GameObject WallEndPoint;

    [SerializeField]
    float resolution;

    [SerializeField]
    UnityEngine.UI.Text debugText;

    // Use this for initialization
    void Start()
    {
        if (Wall == null)
        {
            Debug.LogError("You need to specify a <color=cyan>wall</color> to the object <color=blue>" + this.name + "</color>");
        }
        if (WallEndPoint == null)
        {
            Debug.LogError("You need to specify a <color=cyan>wall end point</color> to the object <color=blue>" + this.name + "</color>");
        }
        if (resolution==0)
        {
            Debug.LogError("You need to specify a <color=cyan>wall resolution</color> end point to the object <color=blue>" + this.name + "</color>");
        }
    }

    // Update is called once per frame

    Ray updateRay;
    RaycastHit hitInfo;
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
            return;
        if(Input.GetMouseButtonUp(1) && isDrawing)
        {
            CancelDraw();
        }
        updateRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(updateRay, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Ground")))
        {
            debugText.text = hitInfo.point.ToString() + "     "+  RoundVector3(hitInfo.point).ToString();

            if (Input.GetMouseButtonUp(0))
            {
                HandleHit(hitInfo);
            }

            if (isDrawing)
            {
                DrawWall(RoundVector3(hitInfo.point));
            }
        }
    }

    private void CancelDraw()
    {
        isDrawing = false;
        Destroy(wallStartingPointGameObject);
    }

    bool isDrawing = false;
    void HandleHit(RaycastHit hitInfo)
    {
        //Debug.Log(hitInfo.point);
        if (isDrawing == false)
        {
            StartDrawing(RoundVector3(hitInfo.point));
        }
        else
        {
            StopDrawing(RoundVector3(hitInfo.point));
        }
    }

    Vector3 RoundVector3(Vector3 vector)
    {
        return new Vector3(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
    }

    Vector3 wallStartingPointPosition;
    GameObject wallStartingPointGameObject;

    void StartDrawing(Vector3 target)
    {
        isDrawing = true;
        wallStartingPointGameObject = (GameObject)Instantiate(WallEndPoint, target, Quaternion.identity);
        wallStartingPointPosition = target;
    }

    void StopDrawing(Vector3 target)
    {
        isDrawing = false;
        //Instantiate(WallEndPoint, target, Quaternion.identity);
    }

    void DrawWall(Vector3 target)
    {
        wallStartingPointGameObject.transform.localScale = new Vector3(Vector3.Magnitude(target - wallStartingPointPosition) * 4,1,1);
        wallStartingPointGameObject.transform.LookAt(target);
        wallStartingPointGameObject.transform.Rotate(new Vector3(0, 270, 0));
    }
}
